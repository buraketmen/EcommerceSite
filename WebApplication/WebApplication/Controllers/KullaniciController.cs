﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class KullaniciController : Controller
    {
        private OyunSatisSitesiEntities db = new OyunSatisSitesiEntities();
        public ActionResult Cikis()
        {
            Session["KullaniciAdi"] = null;
            Session["Admin"] = null;
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Giris()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Giris(Kullanıcı model)
        {
            //var kullanıcı = db.Kullanıcı.FirstOrDefault(x => x.KullanıcıAdı == x.KullanıcıAdı && x.Sifre == model.Sifre);
            var kullanici = db.Kullanıcı.FirstOrDefault(x => x.KullanıcıAdı == model.KullanıcıAdı && x.Sifre == model.Sifre);
            if (kullanici!=null)
            {
                Session["KullaniciAdi"] = kullanici;
                Session["Admin"] = kullanici.Admin;
                return RedirectToAction("Index", "Home");
               
            }
            else
            {
                return View();
            }
        }
       
        public ActionResult Olustur()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Olustur([Bind(Include = "ID,KullanıcıAdı,Sifre,Email,Telefon,Adres,KayıtTarih,İsim,Soyad,Admin")] Kullanıcı kullanici)
        {
            if (ModelState.IsValid)
            {
                if (kullanici.KullanıcıAdı != null && kullanici.Sifre != null)
                {
                    var varol = db.Kullanıcı.FirstOrDefault(x => x.KullanıcıAdı == kullanici.KullanıcıAdı);
                    if (varol != null)
                    {
                        ViewBag.Basarili = "Kullanıcı adı kullanılmaktadır. Lütfen başka bir kullanıcı adı giriniz!";
                    }
                    //var varolan = db.Kullanıcı.Find(kullanici.KullanıcıAdı);
                   
                    else
                    {
                        kullanici.KayıtTarih = DateTime.Now;
                        db.Kullanıcı.Add(kullanici);
                        db.SaveChanges();
                        ViewBag.Basarili = "KAYIT BASARILI!";
                    }
                    
                    return View();
                }
                else return RedirectToAction("Olustur", "Kullanici");
               
                
            }
            return View();
        }
        public ActionResult Hesabim(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kullanıcı kullanici = db.Kullanıcı.Find(id);
            if (kullanici == null)
            {
                return HttpNotFound();
            }
            return View(kullanici);
        }
        public ActionResult BilgiDuzenle(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kullanıcı kullanici = db.Kullanıcı.Find(id);
            if (kullanici == null)
            {
                return HttpNotFound();
            }
            return View(kullanici);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult BilgiDuzenle([Bind(Include = "ID,KullanıcıAdı,Sifre,Email,Telefon,Adres,KayıtTarih,İsim,Soyad,Admin")] Kullanıcı kullanici)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kullanici).State = EntityState.Modified;
                db.SaveChanges();
                Session["KullaniciAdi"] = kullanici;
                ViewBag.Degistir = "Degistirme islemi basarili.";
                return View();
            }
            return View(kullanici);
        }
        public ActionResult Siparislerim(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var siparis1 = db.Siparis1.Include(s => s.Kullanıcı).Include(s => s.Oyun);
            return View(siparis1.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
