﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class SepetController : Controller
    {
        private OyunSatisSitesiEntities db = new OyunSatisSitesiEntities();

        public ActionResult SiparisYolla(int? SepetID, int?id)
        {
                if (SepetID == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Sepet sepet = db.Sepets.Find(SepetID);
                
                if (sepet == null)
                {
                    return HttpNotFound();
                }
                StringBuilder builder = new StringBuilder();
                Random random = new Random();
                char ch;
                for (int i = 0; i < 15; i++)
                {
                    ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                    builder.Append(ch);
                }
                sepet.SepetID = SepetID.Value;

                Siparis sip = new Siparis();
                sip.KisiID = sepet.KisiID;
                sip.OyunID = sepet.OyunID;
                sip.SiparisKodu = builder.ToString();
                sip.Tarih = DateTime.Now;
                db.Siparis1.Add(sip);
                db.Sepets.Remove(sepet);
                
                db.SaveChanges();
                return RedirectToAction("Siparislerim", "Kullanici", new {id = id }); 
             
        }

        public ActionResult SepeteEkle(int? KisiID, int? OyunID)
        {
            if (KisiID== null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
            Sepet urun = new Sepet();

            urun.KisiID = KisiID.Value;
            urun.OyunID = OyunID.Value;

            db.Sepets.Add(urun);
            db.SaveChanges();
            return RedirectToAction("Sepet", "Home");
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sepet sepet = db.Sepets.Find(id);
            sepet.SepetID = id.Value;
            if (sepet == null)
            {
                return HttpNotFound();
            }
            db.Sepets.Remove(sepet);
            db.SaveChanges();
            return RedirectToAction("Sepet","Home");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
