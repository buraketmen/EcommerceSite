﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebApplication.Models;
using System.Data.Entity;
using System.Net;

namespace WebApplication.Controllers
{
    public class HomeController : Controller
    {
        private OyunSatisSitesiEntities db = new OyunSatisSitesiEntities();
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Blog()
        {
            return View();
        }
        public ActionResult Blog2()
        {
            return View();
        }
        public ActionResult Blog3()
        {
            return View();
        }

        public ActionResult Sepet()
        {
            var sepets = db.Sepets.Include(s => s.Kullanıcı).Include(s => s.Oyun);
            return View(sepets.ToList());
        }
        public ActionResult BizKimiz()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ContactUs(FormCollection form)
        {
            OyunSatisSitesiEntities db = new OyunSatisSitesiEntities();
            iletisim ilet = new iletisim();


            ilet.iletisimisim = form["iletisimisim"].Trim();
            ilet.iletisimemail = form["iletisimemail"].Trim();
            ilet.iletisimkonu = form["iletisimkonu"].Trim();
            ilet.iletisimmesaj = form["iletisimmesaj"].Trim();

            db.iletisims.Add(ilet);
            db.SaveChanges();
            return View();

        }
        
     }
}