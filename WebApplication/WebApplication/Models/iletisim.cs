//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApplication.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;

    public partial class iletisim
    {
        public int iletisimID { get; set; }
        public string iletisimisim { get; set; }
        public string iletisimemail { get; set; }
        public string iletisimkonu { get; set; }
        public string iletisimmesaj { get; set; }
    }
}
