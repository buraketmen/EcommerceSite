﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication.Models;

namespace WebApplication.Areas.Admin.Controllers
{
    public class OyunController : Controller
    {
        private OyunSatisSitesiEntities db = new OyunSatisSitesiEntities();

        // GET: Admin/Oyun
        public ActionResult Index()
        {
            if (Session["KullaniciAdi"] == null || Session["Admin"] == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            return View(db.Oyuns.ToList());
        }

        // GET: Admin/Oyun/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oyun oyun = db.Oyuns.Find(id);
            if (oyun == null)
            {
                return HttpNotFound();
            }
            return View(oyun);
        }

        // GET: Admin/Oyun/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Oyun/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "OyunID,OyunAd,OyunFiyat,OyunSatınAlmaTarih,OyunTanıtım,OyunKDV")] Oyun oyun)
        {
            if (ModelState.IsValid)
            {
                db.Oyuns.Add(oyun);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(oyun);
        }

        // GET: Admin/Oyun/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oyun oyun = db.Oyuns.Find(id);
            if (oyun == null)
            {
                return HttpNotFound();
            }
            return View(oyun);
        }

        // POST: Admin/Oyun/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OyunID,OyunAd,OyunFiyat,OyunSatınAlmaTarih,OyunTanıtım,OyunKDV")] Oyun oyun)
        {
            if (ModelState.IsValid)
            {
                db.Entry(oyun).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(oyun);
        }

        // GET: Admin/Oyun/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oyun oyun = db.Oyuns.Find(id);
            if (oyun == null)
            {
                return HttpNotFound();
            }
            return View(oyun);
        }

        // POST: Admin/Oyun/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Oyun oyun = db.Oyuns.Find(id);
            db.Oyuns.Remove(oyun);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
