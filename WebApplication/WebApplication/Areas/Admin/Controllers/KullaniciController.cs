﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication.Models;

namespace WebApplication.Areas.Admin.Controllers
{
    public class KullaniciController : Controller
    {
        private OyunSatisSitesiEntities db = new OyunSatisSitesiEntities();

        // GET: Admin/Kullanıcı
        public ActionResult Index()
        {
            if (Session["KullaniciAdi"] == null || Session["Admin"] == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            return View(db.Kullanıcı.ToList());
        }

        // GET: Admin/Kullanıcı/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kullanıcı kullanici = db.Kullanıcı.Find(id);
            if (kullanici == null)
            {
                return HttpNotFound();
            }
            return View(kullanici);
        }

        // GET: Admin/Kullanıcı/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Kullanıcı/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,KullanıcıAdı,Sifre,Email,Telefon,Adres,KayıtTarih,İsim,Soyad,Admin")] Kullanıcı kullanici)
        {
            if (ModelState.IsValid)
            {
                db.Kullanıcı.Add(kullanici);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(kullanici);
        }

        // GET: Admin/Kullanıcı/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kullanıcı kullanıcı = db.Kullanıcı.Find(id);
            if (kullanıcı == null)
            {
                return HttpNotFound();
            }
            return View(kullanıcı);
        }

        // POST: Admin/Kullanıcı/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,KullanıcıAdı,Sifre,Email,Telefon,Adres,KayıtTarih,İsim,Soyad,Admin")] Kullanıcı kullanici)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kullanici).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(kullanici);
        }

        // GET: Admin/Kullanıcı/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kullanıcı kullanici = db.Kullanıcı.Find(id);
            if (kullanici == null)
            {
                return HttpNotFound();
            }
            return View(kullanici);
        }

        // POST: Admin/Kullanıcı/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Kullanıcı kullanici = db.Kullanıcı.Find(id);
            int i = 0;
            do
            {
                var siparis = db.Siparis1.Where(t => t.KisiID == id).FirstOrDefault();

                var sepet = db.Sepets.Where(t => t.KisiID == id).FirstOrDefault();
                if(siparis !=null )
                {
                    db.Siparis1.Remove(siparis);
                }
                if(sepet != null)
                {
                    db.Sepets.Remove(sepet);
                }
                i++;
                db.SaveChanges();

            } while (i < 10);

            db.Kullanıcı.Remove(kullanici);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
